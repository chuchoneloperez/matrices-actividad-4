﻿using System;

public class MainClass
{
    const int COL_MAX = 5;
    const int ROW_MAX = 5;
    static int[,] MATRIZ = {
        {12,  6,   9,  4,  26},
        {22,  16, 11,  4,   2},
        {56, 890, 23,  4,   6},
        { 2,  12, 91,  4,  11},
        { 1,  16, 19, -4,  99}
    };

    public static void Main(string[] args)
    {
        ImprimirMatriz("0.- TABLA DE CONTENIDO", MATRIZ);
        ImprimirMatrizFormato("1.- TABLA DE CONTENIDO FORMATO", MATRIZ);
        ImprimirTablaPosiciones();
        ImprimirDiagonalInvertida();
        ImprimirDiagonal();
        ImprimirColumna(1);
    }

    static void ImprimirMatriz(string titulo, int[,] matriz)
    {
        Console.WriteLine(titulo);
        for (int i = 0; i < ROW_MAX; i++)
        {
            for (int j = 0; j < COL_MAX; j++)
            {
                Console.Write(matriz[i, j].ToString().PadLeft(4));
            }
            Console.WriteLine();
        }
    }

    static void ImprimirMatrizFormato(string titulo, int[,] matriz)
    {
        Console.WriteLine(titulo);
        for (int i = 0; i < ROW_MAX; i++)
        {
            for (int j = 0; j < COL_MAX; j++)
            {
                Console.Write("|" + matriz[i, j].ToString().PadLeft(4));
            }
            Console.WriteLine("|");
        }
    }

    static void ImprimirTablaPosiciones()
    {
        Console.WriteLine("2.- TABLA DE POSICIONES");
        for (int i = 0; i < ROW_MAX; i++)
        {
            for (int j = 0; j < COL_MAX; j++)
            {
                Console.Write("[{0},{1}] ", i, j);
            }
            Console.WriteLine();
        }
    }

    static void ImprimirDiagonalInvertida()
    {
        Console.WriteLine("3.- DIAGONAL INVERTIDA");
        for (int i = 0; i < ROW_MAX; i++)
        {
            for (int j = 0; j < COL_MAX; j++)
            {
                if (i == j)
                {
                    Console.Write("[" + i + "," + j + "]\t" + "    ");
                }
                else
                {
                    Console.Write("     ");
                }
            }
            Console.WriteLine();
        }
    }

    static void ImprimirDiagonal()
    {
        Console.WriteLine("4.- DIAGONAL /");
        for (int i = 0; i < ROW_MAX; i++)
        {
            Console.WriteLine("[{0},{1}]", i, COL_MAX - 1 - i);
        }
    }

    static void ImprimirColumna(int col)
    {
        Console.WriteLine("5.- COLUMNA {0} |", col + 1);
        for (int i = 0; i < ROW_MAX; i++)
        {
            Console.WriteLine("".PadLeft(10) + "[{0},{1}]", i, col);
        }
        Console.WriteLine();
    }
}
